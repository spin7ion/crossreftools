<?php
function curl_get_file_contents($URL, $sleeps=1)
{
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);

        if ($contents) return $contents;
        else{
        	if($sleeps>0) {
				sleep(60);
				return curl_get_file_contents($URL, $sleeps-1);
			}
			return "";
        } 
}

function getDoi($issn)
{
	preg_match_all('/[0-9]{4}-[0-9]{4}/m', $issn, $matches, PREG_SET_ORDER, 0);

	if(count($matches)==1){
		$issn=$matches[0][0];
	} else {
		return "";
	}

	try{
		$worksPureData=curl_get_file_contents('http://api.crossref.org/journals/'.$issn.'/works?sample=4', 1);
		if($worksPureData==""){
			return "";
		}
		$worksData=json_decode($worksPureData);
		
		if ($worksData == null || !property_exists($worksData, 'message')){			
			return "";
		}

		$arr = explode("/", $worksData->message->items[0]->DOI, 2);
		
		unset($worksPureData);
		unset($worksData);
		return $arr[0];
	} catch (Exception $e){
		echo "error";
		return "";
	}

}

function getTitlePublisher($issn)
{
	preg_match_all('/[0-9]{4}-[0-9]{4}/m', $issn, $matches, PREG_SET_ORDER, 0);

	if(count($matches)==1){
		$issn=$matches[0][0];
	} else {
		return ["",""];
	}

	try{
		$journalPureData=curl_get_file_contents('http://api.crossref.org/journals/'.$issn, 1);
		if($journalPureData==""){
			return ["", ""];
		}
		$journalData=json_decode($journalPureData);
		unset($journalPureData);
		if ($journalData == null){
			return ["",""];
		}
		return [$journalData->message->title, $journalData->message->publisher];
	} catch (Exception $e){
		echo "error";
		return ["",""];
	}

}


$fileName="journals.csv";
$linecount = 0;
$handle = fopen($fileName, "r");
while(!feof($handle)){
  $line = fgets($handle);
  $linecount++;
}

fclose($handle);

echo $linecount;

$row = 1;
$resultHandle = fopen("parsed.csv", "w+");

if (($handle = fopen($fileName, "r")) !== FALSE) {
    while (($line = fgets($handle)) !== FALSE) {
    	echo $row.'/'.$linecount.' '.round($row*100/$linecount,2)."%                                         \t\r";
    	$data = explode(';', $line, 3);
        if(count($data)!=3){
        	echo "Strange line:".$line."\r\n";
        	continue;
    	}
        $row++;
        $issn=$data[0];
        $eissn=$data[1];
        $title=trim($data[2]);

        $doi=getDoi($issn);
        $edoi=getDoi($eissn);
        $tpArr=getTitlePublisher($issn);
        $etpArr=getTitlePublisher($eissn);

        fwrite($resultHandle, $issn.';'.$eissn.';'.$title.';'.$doi.';'.$edoi.';'.$tpArr[0].';'.$tpArr[1].';'.$etpArr[0].';'.$etpArr[1]."\r\n");
    }
    fclose($handle);
}
fclose($resultHandle);
?>