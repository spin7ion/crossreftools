<?php
error_reporting(E_ALL);
if (isset($_GET['issn'])){
	preg_match_all('/[0-9]{4}-[0-9]{4}/m', $_GET['issn'], $matches, PREG_SET_ORDER, 0);

	if(count($matches)==1){
		$issn=$matches[0][0];
	}
}


?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
  	<div class="container">
  	<div class="row">
  		<div class="col-xl">
  		<form>
  		<div class="input-group mb-3">
  			<input type="text" class="form-control" placeholder="ISSN" aria-label="ISSN" aria-describedby="button-addon2" name="issn" value="<?php echo $issn; ?>">
  			<div class="input-group-append">
    			<button class="btn btn-outline-secondary" type="submit" id="button-addon2">Get'em</button>
  			</div>
		</div>
		</form>
	</div>
	</div><div class="row">
		<?php
			function getData($issn){
				try{
					$journalPureData=file_get_contents('http://api.crossref.org/journals/'.$issn);
					$journalData=json_decode($journalPureData);
					unset($journalPureData);
					$worksPureData=file_get_contents('http://api.crossref.org/journals/'.$issn.'/works?sample=4');
					$worksData=json_decode($worksPureData);
					unset($worksPureData);
				} catch (Exception $e){
					?>
					<div class="alert alert-danger" role="alert">
  						A crossref error: <?php echo $journalPureData; ?>
  						<?php var_dump($e); ?>
					</div>
					<?php
					return null;
				}
				
					$arr = explode("/", $worksData->message->items[0]->DOI, 2);
					$prefix = $arr[0];
				?>
				<!-- <?php var_dump($journalData); ?> -->
				<div class="row">
				<p class="h2"><?php echo $journalData->message->publisher. ' ('.$prefix.')'; ?> / <?php echo $journalData->message->title; ?></p><br/>
			</div>
			<div class="row">
				<p class="h5">DOI counts</p><br/>
				<table class="table">
  					<thead>
    					<tr>
      						<th scope="col">Type</th>
      						<th scope="col">Counts</th>
    					</tr>
  					</thead>
  					<tbody>
  						<?php
				foreach ($journalData->message->counts as $key => $value) {
					echo "<tr><td>{$key}</td><td>{$value}</td></tr>";
				}?>
				</tbody></table>
				<p><a class="btn btn-primary" data-toggle="collapse" href="#yearsCollapse" role="button" aria-expanded="false" aria-controls="yearsCollapse">Counts by year</a></p>
				<div class="collapse" id="yearsCollapse"><div class="card card-body"><br>
					<table class="table">
  					<thead>
    					<tr>
      						<th scope="col">Year</th>
      						<th scope="col">Counts</th>
    					</tr>
  					</thead>
  					<tbody>
  						<?php
				foreach ($journalData->message->breakdowns->{'dois-by-issued-year'} as $key => $value) {
					echo "<tr><td>{$value['0']}</td><td>{$value['1']}</td></tr>";
				}?>
				</tbody></table>
  				</div></div>
<?php			
					
			}
			if(isset($issn)){
				getData($issn);
			}
		?>
	</div>
	</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>